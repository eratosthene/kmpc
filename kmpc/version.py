VERSION = (0, 8, 0)
VERSION_STR = ".".join(map(str, VERSION))
VERSION_SHORT = ".".join(map(str, [VERSION[0], VERSION[1]]))
# for buildozer
__version__ = "0.8.0"

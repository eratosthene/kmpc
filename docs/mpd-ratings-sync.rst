.. _mpd-ratings-sync:

##########################
Using ``mpd-ratings-sync``
##########################

``mpd-ratings-sync`` is service that monitors MPD for ratings changes on
files and updates all other tracks with the same identifier accordingly.

This service subscribes to an MPD channel named ‘ratingchange’ and
expects to receive messages on that channel with the URI of a song whose
rating was changed. When it receives such a message, it will query MPD
for that song’s MUSICBRAINZ_TRACKID field (which corresponds to
Recording ID in MusicBrainz) and its rating sticker. It will then set
equivalent rating stickers on all songs in the database with the same
ID.

*****
Usage
*****

::

   usage: mpd-ratings-sync [-h] [-i IP] [-p PORT]

   optional arguments:
     -h, --help            show this help message and exit
     -i IP, --ip IP        ip address or hostname of mpd server (default
                           127.0.0.1)
     -p PORT, --port PORT  port of mpd server (default 6600)

********************
Systemd installation
********************

::

   cat << EOF | sudo tee /etc/systemd/system/mpd-ratings-sync.service
     [Unit]
     Description=MPD Ratings Sync
     After=network.target mpd.service

     [Service]
     ExecStart=/usr/local/bin/mpd-ratings-sync
     Restart=on-failure

     [Install]
     WantedBy=multi-user.target
   EOF
   sudo systemctl daemon-reload
   sudo systemctl start mpd-ratings-sync

Please note that by default the systemd service connects to localhost.
If you need to change that, please edit the above systemd config.

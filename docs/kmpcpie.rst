.. _kmpcpie:

#########################
Car Installation Tutorial
#########################

This document will guide you through a method of setting up a fully functional
touchscreen solution that can be mounted in your car. It uses PipaOS as the
base linux distro.

***********************
Step 1: Install kmpcpie
***********************

#. If you want to build your own, customized disk image, see
   :ref:`build_image` to learn how. Otherwise, download a pre-built image from
   `the kmpc wiki <https://gitlab.com/eratosthene/kmpc/wikis/home>`_.
#. Unzip the downloaded file and flash it to a MicroSD card. I recommend using
   `BalenaEtcher <https://www.balena.io/etcher/>`_. Boot your Pi with this card.
#. You will need to make sure you can SSH to your Pi. To set up wifi, do the
   following:

   #. Power down the Pi and mount the SD card on your desktop.
   #. Edit the file ``wpa_supplicant.txt`` in the root of the SD card.
   #. In one of the **network** blocks, edit the **ssid**, **psk**, and
      **id_str** fields to contain your wifi's name, password, and a nickname
      respectively.
   #. Save the changes, eject the SD card, put it back in the Pi and boot.
#. SSH in to the Pi using username ``sysop`` and password ``posys``. If you
   wish, you can use ``passwd`` to change this password.
#. You'll need to expand your root volume to use the whole SD card. Run::
   
     sudo pipaos-config
   
   choose *Expand Filesystem*, hit enter a few times, let the Pi reboot, then
   log back in.

********************
Step 2: Set up kmpc
********************

Take a look at :ref:`config` and change any config variables you wish. This can
be done by editing the file ``~/.config/kmpc/config.ini`` or through the kmpc
gui.
You'll need to exit kmpc and let it restart to load your changes.

******************
Step 3: Set up mpd
******************

#. Put some mp3 files on a usb drive in a folder named ``Music`` and plug it in
   to your Pi, then reboot to make sure it is mounted.
#. Make sure your audio connection is working. SSH in and Run ``amixer sset
   'PCM' 0`` to turn the audio volume up, then run ``speaker-test`` and listen
   for some output.
#. See https://www.musicpd.org/doc/user/config.html for further details on the
   mpd config file located at ``/usr/local/etc/mpd.conf``. You might want to
   add 'replaygain' variables, for example. If you change anything, restart
   mpd::

     sudo systemctl restart mpd

#. Run the following to update the mpd database::

     mpc update

You should now be able to browse the library, add files to the queue, and
generally use the app.

*********************************
Step 4: Download cache (optional)
*********************************

See :ref:`usingkmpccache` for instructions on using ``kmpccache`` to download
Musicbrainz and Fanart data. If your Pi has internet connectivity, you can run
the script directly on the Pi. However, I prefer to run it on my home machine
and use kmpc's sync functionality to copy it over. See the following section
for those instructions. Once you have a cache folder, you'll need to edit
kmpc's config and change the **cachepath** field in the **[paths]** section to
match, then restart kmpc. You should now see logos and background images for
the artists that have images in the cache folder, as well as more detailed info
about songs, albums, and artists.

*****************************
Step 5: Setup Sync (optional)
*****************************

The basic gist of it is this:

#. Have a Linux box running in your house, connected the same wifi that the car
   Pi will be able to connect to. This will be called the *synchost*.
#. Have mpd running on it, and fully updated.
#. Use ``kmpccache`` to automatically download all the musicbrainz and fanart data.
#. Run ``kmpc`` connected to the *synchost*, and use its ratings and copy_flag
   stickers to generate a synclist. See :ref:`synclist` for more details.
#. Edit kmpc's config on your Pi and change the variables in the [synchost]
   section. See the section on :ref:`config` for details.
#. Run ``ssh-keygen`` on your Pi and hit enter on all the defaults. This
   creates a public key for this user.
#. Insert the contents of ``~/.ssh/id_rsa.pub`` on the Pi into the
   ``~/.ssh/authorized_keys`` file on the *synchost* as whatever user you have
   set up there.
#. Edit the file ``~/.ssh/config`` and add the following::

     Host <synchost>                        # this should match config.ini
       User <synchost_username>             # a user on <synchost>
       StrictHostKeyChecking no

   For example, my ssh config looks like this::

     Host 192.168.1.100
       User cgraham
       StrictHostKeyChecking no

   And the ``synchost`` variable in the ``[sync]`` section in my kmpc config is
   set to "192.168.1.100".

#. Test that your connection is working by running::

     ssh <synchost>

#. Stop the kmpc service and test the sync manually by going to the Config tab
   and clicking Sync::

     systemctl --user stop kmpc
     kmpc

#. If that worked well, exit kmpc and restart the service::

     systemctl --user start kmpc

Now you should be able to use the Sync button in the Config tab to
automatically sync all music, cache, and song ratings with the *synchost*. You
can also run these on the commandline with ``kmpc --sync
<music|cache|importratings|exportratings>``.

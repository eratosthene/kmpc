.. _toc:

################################
Welcome to kmpc's documentation!
################################

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :name: mastertoc

   readme
   install
   invocation
   config
   usingkmpc
   usingkmpccache
   mpd-ratings-sync
   kmpcpie
   build_image
   plugins
   changelog


#!/bin/bash

olddir="$1"
newdir="$2"

for artist_folder in "$olddir"/*; do
  echo "Migrating fanart from $artist_folder"
  artist_id=`basename "$artist_folder"`
  t1=${artist_id:0:2}
  t2=${artist_id:2:2}
  t3=${artist_id:4:2}
  t4=${artist_id:6:2}
  newpath="$newdir/artist/$t1/$t2/$t3/$t4/$artist_id"
  echo "Migrating fanart to $newpath"
  if [ -d "$newpath" ]; then
    if [ -d "$artist_folder/logo" ]; then
      mv -v "$artist_folder/logo" "$newpath"
    fi
    if [ -d "$artist_folder/artistbackground" ]; then
      mv -v "$artist_folder/artistbackground" "$newpath"
    fi
    if [ -d "$artist_folder/badge" ]; then
      mkdir -p "$newpath/logo"
      mv -v "$artist_folder/badge/"* "$newpath/logo"
    fi
    rm -rf "$artist_folder"
  fi
done

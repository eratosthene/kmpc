.. _build_image:

##################################
Building a Raspberry Pi Boot Image
##################################

In the scripts/build_image folder, you will find a set of scripts intended to
create a bootable SD card image for the Raspberry Pi. The image is based on
`PipaOS <http://pipaos.mitako.eu/>`_ 6.0, which is itself based on Debian
Stretch. `XSysroot <http://xsysroot.mitako.eu/>`_ is required to be installed
and set up, so please read the instructions there. The script will only run on a
Linux host, and has only really been tested on Ubuntu 16.04. The ``build.sh``
script performs the following functions:

#. Extracts the base PipaOS image and makes a few modifications.
#. Installs `Kivy <https://kivy.org/>`_ version 1.10.1 from source and all of
   its dependencies.
#. Installs `libmpdclient <https://www.musicpd.org/libs/libmpdclient/>`_ 2.16,
   `mpc <https://www.musicpd.org/clients/mpc/>`_ 0.31, and `mpd
   <https://www.musicpd.org/download.html>`_ 0.21.8 from source and all of their
   respective dependencies, and sets mpd to run at boot.
#. Installs the piswitch service to handle system power. This interfaces with
   the `Pi Supply Switch
   <https://uk.pi-supply.com/products/pi-supply-raspberry-pi-power-switch>`_
   which is a great way to handle shutting down the Pi cleanly. If you don't
   have this hardware, comment out that section in build.sh.
#. Installs the latest version of kmpc, and sets mpd-ratings-sync and kmpc to
   run at boot.

At the end of the run, you should have a file named kmpcpie-1.0.img, which can
be flashed to an SD card and booted on the Pi.

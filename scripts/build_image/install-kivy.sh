#!/bin/bash
ARM_MUST_BE='armv7l'
mach=`uname -m`
if [ "$mach" != "$ARM_MUST_BE" ]; then
    echo "Careful! Looks like I am not running inside an ARM system!"
    echo "uname -m is telling me: $mach"
    exit 1
else
    # Avoid errors from stopping the script
    set +e
fi

# If no Kivy source release is provided
# install the latest unstable master branch
if [ "$1" == "" ]; then
    kivy_release="master"
else
    kivy_release="$1"
fi

# List of packages needed to build Kivy and additional tools

BUILD_PKGS="
pkg-config libgl1-mesa-dev libgles2-mesa-dev
python-pygame python-setuptools python3-setuptools libgstreamer1.0-dev git-core
gstreamer1.0-plugins-bad gstreamer1.0-plugins-base
gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly
gstreamer1.0-omx gstreamer1.0-alsa python-dev python3-dev
vim vim-python-jedi emacs python-mode mc
libmtdev1 libmtdev-dev
xclip xsel
libpng12-dev
zip unzip sshfs
libsdl2-dev
libsdl2-gfx-dev
libsdl2-image-dev
libsdl2-mixer-dev
libsdl2-net-dev
libsdl2-ttf-dev
libpcre3 libfreetype6
fonts-freefont-ttf dbus
python-docutils
libssl1.0.2 libsmbclient libssh-4
python-rpi.gpio python3-rpi.gpio raspi-gpio wiringpi
libraspberrypi-dev wget
libvncserver-dev
python-beautifulsoup
python3-bs4
libffi-dev
libssl-dev
"

echo ">>> Kivy build process starts at: `date`"

# Install dependencies
echo ">>> Installing dependencies"
apt-get install -y --force-yes $BUILD_PKGS
apt-get autoclean

# upgrade to latest PIP for python 2 and 3
echo ">>> Installing pip and pip3"
easy_install3 -U pip
easy_install -U pip

# Stop services started due to the installation
/etc/init.d/dbus stop

echo ">>> Build and install Cython"
pip install cython==0.28.2
pip3 install cython==0.28.2

echo ">>> Installing pygments"
pip install pygments
pip3 install pygments

# get Kivy source code
kivy_url="https://github.com/kivy/kivy/archive/$kivy_release.zip"
sourcezip=/tmp/kivy_source.zip
cd /tmp

# cleanup previous build
rm -fv $sourcezip
rm -rfv /tmp/kivy-$kivy_release

# download and unzip sources
echo ">>> Downloading kivy source code url: $kivy_url"
curl -s -L $kivy_url > $sourcezip
unzip -o $sourcezip

# build kivy for python 3
echo ">>> PIP3 is building kivy for Python 3...."
cd /tmp/kivy-$kivy_release
pip3 install --upgrade .

# cleanup previous kivy build for python 3
cd /tmp
rm -rfv /tmp/kivy-$kivy_release
unzip -o $sourcezip

# now build kivy for python 2
echo ">>> PIP is building kivy for Python 2...."
cd /tmp/kivy-$kivy_release
pip install --upgrade .

echo ">>> Setting Kivy to point to Python3"
ln -sfv /usr/bin/python3 /usr/bin/kivy
kivy -V

echo ">>> Kivy build process finished at: `date`"
exit 0


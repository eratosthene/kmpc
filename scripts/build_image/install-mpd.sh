#!/bin/bash

BUILD_PKGS="
g++
libao-dev libasound2-dev libaudiofile-dev
libavcodec-dev libavformat-dev libavutil-dev
libbz2-dev libboost-dev libcurl4-gnutls-dev
libexpat1-dev libfaad-dev libflac-dev
libglib2.0-dev libicu-dev libid3tag0-dev
libmad0-dev libmikmod2-dev libmodplug-dev
libmp3lame-dev libmpdclient-dev libmpg123-dev
libsamplerate0-dev
libsndfile-dev
libsqlite3-dev
libsystemd-dev
libvorbis-dev
ninja-build
"

echo ">>> MPD build process starts at: `date`"

# Install dependencies
echo ">>> Installing dependencies"
apt-get install -y --force-yes $BUILD_PKGS
apt-get autoclean

echo ">>> Installing meson"
pip3 install meson

# Stop services started due to the installation
/etc/init.d/dbus stop

cd /tmp

lmc_version=2.18
lmc_url=https://www.musicpd.org/download/libmpdclient/2/libmpdclient-$lmc_version.tar.xz
sourcezip=/tmp/libmpdclient-$lmc_version.tar.xz
# cleanup previous build
rm -fv $sourcezip
rm -fv /tmp/libmpdclient-$lmc_version

echo ">>> Downloading libmpdclient source code url: $lmc_url"
cd /tmp
curl -s -L $lmc_url > $sourcezip
tar --overwrite -xvf $sourcezip

echo ">>> Building libmpdclient"
cd /tmp/libmpdclient-$lmc_version
meson . output
ninja -C output
ninja -C output install

mpc_version=0.33
mpc_url=https://www.musicpd.org/download/mpc/0/mpc-$mpc_version.tar.xz
sourcezip=/tmp/mpc-$mpc_version.tar.xz
# cleanup previous build
rm -fv $sourcezip
rm -fv /tmp/mpc-$mpc_version

echo ">>> Downloading mpc source code url: $mpc_url"
cd /tmp
curl -s -L $mpc_url > $sourcezip
tar --overwrite -xvf $sourcezip

echo ">>> Building mpc"
cd /tmp/mpc-$mpc_version
meson . output
ninja -C output
ninja -C output install

mpd_version=0.21.24
mpd_short_version=0.21
mpd_url=https://www.musicpd.org/download/mpd/$mpd_short_version/mpd-$mpd_version.tar.xz
sourcezip=/tmp/mpd-$mpd_version.tar.xz
# cleanup previous build
rm -fv $sourcezip
rm -fv /tmp/mpd-$mpd_version

echo ">>> Downloading mpd source code url: $mpd_url"
cd /tmp
curl -s -L $mpd_url > $sourcezip
tar --overwrite -xvf $sourcezip

echo ">>> Building mpd"
cd /tmp/mpd-$mpd_version
meson . output
ninja -C output
ninja -C output install

echo ">>> Creating initial folder layout"
useradd -M mpd
usermod -L mpd
usermod -G audio mpd
mkdir -p /var/lib/mpd/playlists
mkdir -p /var/log/mpd
chown -R mpd:audio /var/lib/mpd
chown -R mpd:audio /var/log/mpd
# if you have your music folder mounted somewhere else, edit this block
cat << EOF > /usr/local/etc/mpd.conf
music_directory         "/mnt/usb/Music"
playlist_directory      "/var/lib/mpd/playlists"
db_file                 "/var/lib/mpd/database"
log_file                "/var/log/mpd/mpd.log"
pid_file                "/var/lib/mpd/pid"
state_file              "/var/lib/mpd/state"
sticker_file            "/var/lib/mpd/sticker.sql"
user                    "mpd"
group                   "audio"
bind_to_address         "127.0.0.1"
max_output_buffer_size  "32768"

audio_output {
    type "alsa"
    name "system audio"
    device "hw:0,0"
}
EOF

# if you don't have a usb stick with your music on it, comment this block
echo "/dev/sda1       /mnt/usb        vfat    defaults,noatime,rw,uid=sysop,gid=audio,umask=002,utf8,iocharset=utf8   0       0" >> /etc/fstab

# start mpd at boot
systemctl enable mpd.service

echo ">>> MPD build process finished at: `date`"
exit 0

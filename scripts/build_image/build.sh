#!/bin/bash

VERSION=1.1
KIVY_VERSION=1.10.1
OUTPUT_IMAGE=kmpcpie-$VERSION.img
XSYSROOT_PROFILE=kmpcpie
XSYSROOT="xsysroot -p $XSYSROOT_PROFILE"

# xsysroot setup
cp xsysroot.conf ~
mkdir -p ~/osimages
mkdir -p ~/xtmp
sudo chmod 777 ~/xtmp

echo ">>> Preparing disk image..."
# if image is mounted, unmount it
if [[ "`$XSYSROOT -i`" == *"True"* ]]; then
  $XSYSROOT -u
fi
# renew image so we start from scratch
$XSYSROOT -r
# expand image
$XSYSROOT -u
$XSYSROOT -e
# and mount it
$XSYSROOT -m

echo ">>> Updating OS..."
# update system
$XSYSROOT -x "apt-get update"

# get some variables
SYSROOT=`$XSYSROOT -q sysroot`
SYSBOOT=`$XSYSROOT -q sysboot`
SYSTMP=`$XSYSROOT -q tmp`
QCOW_IMAGE=`$XSYSROOT -q qcow_image`

echo ">>> Copying files to disk image..."
# copy boot customizations
sudo cp -fv config.txt $SYSBOOT
sudo cp -fv cmdline.txt $SYSBOOT

# copy installation scripts
sudo cp -fv install-kivy.sh $SYSTMP
sudo cp -fv install-mpd.sh $SYSTMP
sudo cp -fv install-piswitch.sh $SYSTMP
sudo cp -fv install-kmpc.sh $SYSTMP

echo ">>> Installing Kivy..."
# install kivy
$XSYSROOT -x "/bin/bash -c 'cd /tmp; ./install-kivy.sh $KIVY_VERSION'"

echo ">>> Installing MPD..."
# install mpd
$XSYSROOT -x "/bin/bash -c 'cd /tmp; ./install-mpd.sh'"

echo ">>> Installing piswitch..."
# install piswitch
$XSYSROOT -x "/bin/bash -c 'cd /tmp; ./install-piswitch.sh'"

echo ">>> Installing kmpc..."
# install kmpc
$XSYSROOT -x "/bin/bash -c 'cd /tmp; ./install-kmpc.sh'"

echo ">>> Final adjustments and clean up..."
sudo rm -rf $SYSTMP/*
$XSYSROOT -x "systemctl disable getty@tty1"
echo 'SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"' | sudo tee -a $SYSROOT/etc/udev/rules.d/backlight-permissions.rules
sudo mkdir -p $SYSROOT/home/sysop/.kivy
sudo mkdir -p $SYSROOT/root/.kivy
sudo cp config.ini $SYSROOT/home/sysop/.kivy
sudo cp config.ini $SYSROOT/root/.kivy
$XSYSROOT -x "chown -R sysop:sysop /home/sysop"
sudo cp pipaos-kmpc.png $SYSROOT/usr/share/pipaos-tools/logos/
sudo ln -sf pipaos-kmpc.png $SYSROOT/usr/share/pipaos-tools/logos/default.png

echo ">>> Converting disk image to flashable format..."
$XSYSROOT -u
rm -f $OUTPUT_IMAGE
qemu-img convert "$QCOW_IMAGE" "$OUTPUT_IMAGE"

#!/bin/bash

echo ">>> kmpc build process starts at: `date`"

echo ">>> Installing kmpc and dependencies..."
# install kmpc and dependencies
pip3 install --upgrade kmpc[rpi]

# setup systemd services
cat << EOF > /etc/systemd/system/mpd-ratings-sync.service
  [Unit]
  Description=MPD Ratings Sync
  After=network.target mpd.service

  [Service]
  ExecStart=/usr/local/bin/mpd-ratings-sync
  Restart=on-failure

  [Install]
  WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable mpd-ratings-sync

# setup kmpc as a user process to make it less crashy
mkdir -p /home/sysop/.config/systemd/user
cat << EOF > /home/sysop/.config/systemd/user/kmpc.service
[Unit]
Description=kmpc

[Service]
ExecStart=/usr/local/bin/kmpc
Restart=always

[Install]
WantedBy=default.target
EOF
chown -R sysop:sysop /home/sysop/.config
sudo -i -u sysop systemctl --user daemon-reload
sudo -i -u sysop systemctl --user enable kmpc

# allow sysop user to stay logged in
mkdir -p /var/lib/systemd/linger
touch /var/lib/systemd/linger/sysop

# generate initial kmpc config
mkdir -p /home/sysop/.config/kmpc
cat << EOF > /home/sysop/.config/kmpc/config.ini
[mpd]
mpdhost = 127.0.0.1
mpdport = 6600

[paths]
tmppath = /tmp
cachepath = /home/sysop/cache
musicpath = /mnt/usb/Music

[sync]
synccachepath = /mnt/mb-cache
synctmppath = /tmp
synchost = 127.0.0.1
syncmpdport = 6600
syncmusicpath = /mnt/music
syncplaylist = synclist

[system]
exportfirst = 1
rpienable = 1
advancedtitles = 1
poweroffcommand = sudo poweroff
rebootcommand = sudo reboot
originalyear = 1
updatecommand = sudo pip3 install -U kmpc --no-deps

[songratings]
star10 = Favorite songs of all time
star5 = Filler tracks with no music
star4 = Songs someone else likes
star7 = Occasional listening songs
star6 = Meh track or short musical filler
star1 = Songs that should never be heard
star0 = Silence
star3 = Songs for certain occasions
star2 = Songs no one likes
star9 = Best songs by an artist
star8 = Great songs for all occasions

[colors]
listitemselected = #FFFF00
listitemcurrent = #521C4F
textoutline = #000000
textcolor = #FFFFFF
textshadow = #000000
button = #00B361
listitem = #4080FF
outlinetype = 0
outlinewidth = 1
textshadowsize = 4
backdrop = #4096FF
EOF

echo ">>> kmpc build process finished at: `date`"
exit 0
